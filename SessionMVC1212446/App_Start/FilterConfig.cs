﻿using System.Web;
using System.Web.Mvc;

namespace SessionMVC1212446
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}